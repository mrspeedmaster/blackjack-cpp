//
//  Game.cpp
//  BlackJack
//
//  Created by Bruno Balic on 27/05/2019.
//  Copyright © 2019 Bruno Balic. All rights reserved.
//

#include "Game.hpp"

Game::Game(int numOfPlayers) {
    this->numberOfPlayers = numOfPlayers;
    this->dealer = Player("Dealer");
    for (int i = 0; i < numOfPlayers; i++) {
        this->playersStatus.push_back(1);
    }
}

void Game::setPlayersNames() {
    for (int i = 0; i < this->numberOfPlayers; i++) {
        std::cout << "Ime " << i + 1 << ". igraca?" << std::endl;
        std::string tmp;
        std::cin >> tmp;
        this->players.push_back(Player(tmp));
    }
}

int Game::getNumOfPlayers() {
    return this->numberOfPlayers;
}

void Game::printTable(bool dealerFull) {
    if (dealerFull) {
        std::cout << "Dealer:" << std::endl;
        this->dealer.printHand();
        std::cout << "sum low: " << this->dealer.getSumLow() << " ; sum high " << this->dealer.getSumHigh() << std::endl << std::endl;
        std::cout << std::endl;
    }
    else {
        std::cout << "Dealer:" << std::endl;
        this->dealer.printHandDealer();
        std::cout << std::endl;
    }
    
    for (int i = 0; i < this->numberOfPlayers; i++) {
        std::cout << "Player: " << this->players.at(i).getName() << std::endl;
        this->players.at(i).printHand();
        std::cout << "sum low: " << players.at(i).getSumLow() << " ; sum high " << players.at(i).getSumHigh() << std::endl << std::endl;
    }
}

void Game::printPlayerHand(int playerIndex) {
    this->players.at(playerIndex).printHand();
    std::cout << "sum low: " << players.at(playerIndex).getSumLow() << " ; sum high " << players.at(playerIndex).getSumHigh() << std::endl << std::endl;
}

void Game::resetPlayersStatus() {
    for (int i = 0; i < this->numberOfPlayers; i++) {
        this->playersStatus.at(i) = 1;
    }
}

void Game::setPlayerStatus(int index) {
    this->playersStatus.at(index) = 0;
}

void Game::resetPlayersHand() {
    this->dealer.removeCardsInHand();
    for (int i = 0; i < this->numberOfPlayers; i++) {
        this->players.at(i).removeCardsInHand();
        this->playersStatus.at(i) = 0;
    }
}

void Game::Deal() {
    this->resetPlayersStatus();
    int cardIndex = 0;
    for (int i = 0; i < this->numberOfPlayers; i++) {
        this->players.at(i).addCardToHand(this->deck.at(cardIndex));
        cardIndex++;
    }
    this->dealer.addCardToHand(this->deck.at(cardIndex));
    cardIndex++;
    for (int i = 0; i < this->numberOfPlayers; i++) {
        this->players.at(i).addCardToHand(this->deck.at(cardIndex));
        cardIndex++;
    }
    this->dealer.addCardToHand(this->deck.at(cardIndex));
    cardIndex++;
    this->cardIndex = cardIndex;
}

void Game::DealAdditional(int playerIndex) {
    this->players.at(playerIndex).addCardToHand(this->deck.at(this->cardIndex));
    this->cardIndex++;
}

void Game::DealAdditionalDealer() {
    this->dealer.addCardToHand(this->deck.at(this->cardIndex));
    this->cardIndex++;
}

std::string Game::getPlayerNameAtIndex(int index) {
    return this->players.at(index).getName();
}

int Game::getHandSumLowForPlayer(int playerIndex) {
    return (this->players.at(playerIndex).getSumLow());
}

int Game::getHandSumLowForDealer() {
    return (this->dealer.getSumLow());
}

int Game::getHandSumHighForDealer() {
    return (this->dealer.getSumHigh());
}
