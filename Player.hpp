//
//  Player.hpp
//  BlackJack
//
//  Created by Bruno Balic on 27/05/2019.
//  Copyright © 2019 Bruno Balic. All rights reserved.
//

#ifndef Player_hpp
#define Player_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include "Card.hpp"

class Player {
    std::string name;
    std::vector<Card> cardsInHand;
    int cardsInHandIndex;
    int currentSumLow; // ace 1
    int currentSumHigh; // are 11
    
public:
    Player();
    Player(std::string name);
    
    void setName(std::string name);
    
    std::string getName();
    
    int getSumLow();
    int getSumHigh();
    
    void addCardToHand(Card card);
    
    void removeCardsInHand();
    
    void printHand();
    
    void printHandDealer();
};

#endif /* Player_hpp */
