//
//  main.cpp
//  BlackJack
//
//  Created by Bruno Balic on 27/05/2019.
//  Copyright © 2019 Bruno Balic. All rights reserved.
//

#include <iostream>
#include "Deck.hpp"
#include "Game.hpp"
#include "Player.hpp"

int main(int argc, const char * argv[]) {
    /*
    Deck x;    
    for (int i = 0; i < 52; i++) {
        x.deck.at(i).printDecoded();
    }
    
    x.Shuffle();
    
    for (int i = 0; i < 52; i++) {
        x.deck.at(i).printDecoded();
    }
    */
    
    int playersTmp;
    std::cout << "Koliko igraca zeli igrati?" << std::endl;
    std::cin >> playersTmp;
    Game x1(playersTmp);
    x1.setPlayersNames();
    
    char playNext = 'y';
    while (playNext == 'y') {
        
        x1.resetPlayersHand();
        
        x1.Shuffle();
        x1.Deal();
        
        std::cout << "//////////" << std::endl;
        x1.printTable(false); // printa samo jednu dealerovu kartu
        std::cout << "//////////" << std::endl;
        
        char playMore;
        for (int i = 0; i < x1.getNumOfPlayers(); i++) {
            playMore = 'd';
            while (playMore == 'd') {
                if (x1.players.at(i).getSumLow() != 21 && x1.players.at(i).getSumHigh() != 21) {
                    
                    std::cout << "Igrac: " << x1.getPlayerNameAtIndex(i) <<  ", Dalje / Stop? (d/s)" << std::endl;
                    x1.printPlayerHand(i);
                    
                    std::cin >> playMore;
                    
                    if (playMore == 'd') {
                        x1.DealAdditional(i);
                        x1.printPlayerHand(i);
                    }
                    if (x1.getHandSumLowForPlayer(i) > 21) {
                        std::cout << "Gubitak, resultat veci od 21!" << std::endl;
                        break;
                    }
                }
                else {
                    break;
                }
            }
        }
        
        while ((x1.getHandSumLowForDealer() < 17) && (x1.getHandSumHighForDealer() < 18)) {
            x1.DealAdditionalDealer();
            
            x1.dealer.printHand();
            std::cout << "sum low: " << x1.dealer.getSumLow() << " ; sum high " << x1.dealer.getSumHigh() << std::endl << std::endl;
            
            if (x1.getHandSumLowForDealer() > 21) {
                std::cout << "Gubitak, resultat veci od 21!" << std::endl << std::endl;
                break;
            }
        }
        
        std::cout << "Dealer: " << std::endl;
        x1.dealer.printHand();
        std::cout << "sum low: " << x1.dealer.getSumLow() << " ; sum high " << x1.dealer.getSumHigh() << std::endl << std::endl;
        
        
        std::cout << "//////////" << std::endl;
        x1.printTable(true); // printa sve dealerove karte
        std::cout << "//////////" << std::endl;
        
        std::cout << "Play next hand? (y/n)" << std::endl;
        std::cin >> playNext;
    }
        
    return 0;
}
