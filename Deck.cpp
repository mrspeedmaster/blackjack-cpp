//
//  Deck.cpp
//  BlackJack
//
//  Created by Bruno Balic on 27/05/2019.
//  Copyright © 2019 Bruno Balic. All rights reserved.
//

#include "Deck.hpp"

Deck::Deck() {
    //this->deck.resize(52);
    int i = 17;
    unsigned char broj;
    for (int j = 0; j < 52; j++) {
        broj = (i/16) * 16;
        broj += i % 16;
        //spil[j].c = broj;
        this->deck.push_back(Card(broj));
        i++;
        if (i%16 > 13) {
            i = i + 3;
        }
        //x[i-17].c = broj;
    }
}

void Deck::Shuffle() {
    std::random_device rd;
    std::mt19937 g(rd());
    std::shuffle(this->deck.begin(), this->deck.end(), g);
}
