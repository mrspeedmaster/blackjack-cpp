//
//  Game.hpp
//  BlackJack
//
//  Created by Bruno Balic on 27/05/2019.
//  Copyright © 2019 Bruno Balic. All rights reserved.
//

#ifndef Game_hpp
#define Game_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include "Deck.hpp"
#include "Player.hpp"

class Game: public Deck {
    int cardIndex;
    int numberOfPlayers;
    //std::vector<Player> players;
    std::vector<int> playersStatus;
    //Player dealer;
    
public:
    std::vector<Player> players;
    Player dealer;
    
public:
    Game(int numOfPlayers);
    void setPlayersNames();
    int getNumOfPlayers();
    
    void printTable(bool dealerFull);
    void printPlayerHand(int playerIndex);
    
    void resetPlayersStatus();
    void setPlayerStatus(int index);
    
    void resetPlayersHand();
    
    void Deal();
    void DealAdditional(int playerIndex);
    void DealAdditionalDealer();
    
    std::string getPlayerNameAtIndex(int index);
    
    int getHandSumLowForPlayer(int playerIndex);
    int getHandSumLowForDealer();
    int getHandSumHighForDealer();
};

#endif /* Game_hpp */
