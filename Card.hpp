//
//  Card.hpp
//  BlackJack
//
//  Created by Bruno Balic on 27/05/2019.
//  Copyright © 2019 Bruno Balic. All rights reserved.
//

#ifndef Card_hpp
#define Card_hpp

#include <stdio.h>
#include <iostream>

class Card {
    unsigned char card;
    
public:
    Card(unsigned char value);
    unsigned char getValue();
    unsigned char getSuit();
    void printDecoded();
};

#endif /* Card_hpp */
