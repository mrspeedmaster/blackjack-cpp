//
//  Deck.hpp
//  BlackJack
//
//  Created by Bruno Balic on 27/05/2019.
//  Copyright © 2019 Bruno Balic. All rights reserved.
//

#ifndef Deck_hpp
#define Deck_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <algorithm>
#include <random>
#include <chrono>
#include "Card.hpp"

class Deck {
public:
    std::vector<Card> deck;
    
public:
    Deck();
    void Shuffle();
};

#endif /* Deck_hpp */
