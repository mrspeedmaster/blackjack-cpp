//
//  Player.cpp
//  BlackJack
//
//  Created by Bruno Balic on 27/05/2019.
//  Copyright © 2019 Bruno Balic. All rights reserved.
//

#include "Player.hpp"

Player::Player() {
    
}

Player::Player(std::string Name) {
    this->name = Name;
    //this->cardsInHand.resize(10);
    Card x(1);
    for (int i = 0; i < 12; i++) {
        this->cardsInHand.push_back(x);
    }
    this->cardsInHandIndex = 0;
}

void Player::setName(std::string Name) {
    this->name = Name;
    //this->cardsInHand.resize(10);
    Card x(1);
    for (int i = 0; i < 12; i++) {
        this->cardsInHand.push_back(x);
    }
    this->cardsInHandIndex = 0;
}

std::string Player::getName() {
    return this->name;
}

int Player::getSumLow() {
    int tmp = 0;
    for (int i = 0; i < this->cardsInHandIndex; i++) {
        if (this->cardsInHand.at(i).getValue() == 1) { // ace
            tmp += 1;
        }
        else {
            if (cardsInHand.at(i).getValue() < 11) {
                tmp += cardsInHand.at(i).getValue();
            }
            else {
                tmp += 10;
            }
            //tmp+= (10 * (cardsInHand.at(i).getValue() / 10)) + (cardsInHand.at(i).getValue() % 10);
        }
    }
    this->currentSumLow = tmp;
    return tmp;
}

int Player::getSumHigh() {
    int tmp = 0;
    int aces = 0;
    for (int i = 0; i < this->cardsInHandIndex; i++) {
        if (this->cardsInHand.at(i).getValue() == 1) { // ace
            if (aces > 1) { // ako su 2 ili vise asova, racuna ih kao 1
                tmp += 1;
            }
            else {
                tmp += 11;
                aces++;
            }
        }
        else {
            if (cardsInHand.at(i).getValue() < 11) {
                tmp += cardsInHand.at(i).getValue();
            }
            else {
                tmp += 10;
            }
            //tmp+= (10 * (cardsInHand.at(i).getValue() / 10)) + (cardsInHand.at(i).getValue() % 10);
        }
    }
    this->currentSumHigh = tmp;
    return tmp;
}

void Player::addCardToHand(Card card) {
    //this->cardsInHand.push_back(card);
    this->cardsInHand.at(this->cardsInHandIndex) = card;
    this->cardsInHandIndex++;
}

void Player::removeCardsInHand() {
    this->cardsInHandIndex = 0;
}

void Player::printHand() {
    for (int i = 0; i < this->cardsInHandIndex; i++) {
        this->cardsInHand.at(i).printDecoded();
        std::cout << std::endl;
    }
}

void Player::printHandDealer() {
    this->cardsInHand.at(1).printDecoded();
    std::cout << std::endl;
}
