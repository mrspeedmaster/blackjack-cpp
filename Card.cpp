//
//  Card.cpp
//  BlackJack
//
//  Created by Bruno Balic on 27/05/2019.
//  Copyright © 2019 Bruno Balic. All rights reserved.
//

#include "Card.hpp"

Card::Card(unsigned char value) {
    this->card = value;
}

unsigned char Card::getValue() {
    return this->card % 16;
}

unsigned char Card::getSuit() {
    return (this->card/16)+64; // a b c d
    return this->card/16; // 1 2 3 4
}

void Card::printDecoded() {
    std::cout << (char)((this->card / 16) + 64) << " " << this->card % 16;
}
